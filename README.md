# data_landfill

Generate large amounts of fake data and dump to csv files (compressed with gzip by default).
  
## Requirements
- Python 2.7
- faker (pip install fake-factory)
  
## BasicUsage
Write project specific code where a table is represented by an OrderedDict where the values should be generalize callable methods (must not take any parameters) that return the desired fake data.
E.g.:

    fact_events = OrderedDict([("Event_Date", rand_datetime),
                           ("Event_Id", rand_event_id),
                           ("Account_Id", rand_account_id),
                           ("Ip_Address", fake.ipv4),
                           ("User_Agent", fake.user_agent),
                           ("URI", rand_uri_path),
                           ("Result", rand_result)])

The callables should be defined as functions that don't take parameters, such as:

    def rand_uri_path():
        return fake.uri_path() + fake.uri_page() + fake.uri_extension()

Instantiate a ``Landfill`` object with the OrderedDict, desired file prefix, and row_count.
Finally, call the ``generate_csv()`` method to write gzip compressed csv files to your current directory.

    factevents = Landfill(fact_events,
                          file_prefix='Fact_Events',
                          row_count=1000000)
    factevents.generate_csv()

Write normal text files by passing compress=False to the ``generate_csv`` method

    factevents.generate_csv(compress=False)
