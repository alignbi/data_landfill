"""Variables and Functions and main function specific to the snowball POC."""

import time
import random
from collections import OrderedDict
from faker import Factory
from data_landfill.landfill import Landfill


fake = Factory.create()


# Generic infinite iterator
def increment_gen(start=1, step=1):
    n = start
    while True:
        yield n
        n += step


# Variables and functions for returning desired fake data
# Items for events fact table
results = ['200'] * 90 + ['503'] * 5 + ['500'] * 5

def rand_uri_path():
    return fake.uri_path() + fake.uri_page() + fake.uri_extension()


def rand_datetime():
    return fake.date_time_this_month().isoformat(' ')


def rand_account_id():
    return random.randint(1, 1000000)


def rand_event_id():
    return random.randint(1, 100)


def rand_result():
    return random.choice(results)


# Items for the accounts dimension table
account_id_gen = increment_gen()

def account_id_incr():
    return next(account_id_gen)


# Items for the events dimension table
events = ["click", "view", "authentication success", "authentication failure", "session start", "session end"]
event_categories = []

for n in range(1, 101):
    events.append("event" + str(n))
    event_categories.append("category" + str(n))

event_id_gen = increment_gen()
event_index_gen = increment_gen(start=0)
event_category_index_gen = increment_gen(start=0)


def event_id_incr():
    return next(event_id_gen)


def event_name():
    return events[next(event_index_gen)]


def event_category():
    return event_categories[next(event_category_index_gen)]


# Table dicts. Values must be callable functions/methods.
fact_events = OrderedDict([("Event_Date", rand_datetime),
                           ("Event_Id", rand_event_id),
                           ("Account_Id", rand_account_id),
                           ("Ip_Address", fake.ipv4),
                           ("User_Agent", fake.user_agent),
                           ("URI", rand_uri_path),
                           ("Result", rand_result)])

dim_accounts = OrderedDict([("Account_Id", account_id_incr),
                            ("Account_Uuid", fake.uuid4),
                            ("Account_Name", fake.name),
                            ("Account_Country", fake.country)])

dim_events = OrderedDict([("Event_Id", event_id_incr),
                          ("Event_Name", event_name),
                          ("Event_Category", event_category)])


def main():
    factevents = Landfill(fact_events,
                          file_prefix='Fact_Events',
                          row_count=1000000)

    dimaccounts = Landfill(dim_accounts,
                           file_prefix='Dim_Accounts',
                           row_count=1000000)

    dimevents = Landfill(dim_events,
                         file_prefix='Dim_Events',
                         row_count=100)

    for table in [dimevents, dimaccounts, factevents]:
        start_time = time.clock()
        table.generate_csv()
        elapsed_time = time.clock() - start_time
        print("Time Spent: " + str(elapsed_time) + " seconds")


if __name__ == "__main__":
    main()
