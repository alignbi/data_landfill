"""Defines Landfill Object."""

import csv
import gzip


class Landfill:
    """
    A Landfill object takes an OrderedDict (table_dict) that represents a
    table's column names where the values should be generalize callable methods
    that return the desired fake data.
    """
    def __init__(self,
                 table_dict,
                 file_prefix=None,
                 row_count=0,
                 byte_chunk_size=104857600):
        self.file_prefix = file_prefix
        self.table_dict = table_dict
        self.row_count = row_count
        self.byte_chunk_size = byte_chunk_size

    def _dict_factory(self):
        while self.row_count > 0:
            newdict = {}
            for key in self.table_dict.keys():
                newdict[key] = self.table_dict[key]()
            yield newdict
            self.row_count -= 1

    def generate_csv(self, compress=True):
        """
        Executes methods specified in the instance's table_dict and writes the
        resulting csv file(s). By default, compresses the files with gzip.
        Write non-compressed files with ``compress=False``
        """
        field_names = self.table_dict.keys()
        csv_num = 0
        df = self._dict_factory()
        writing_rows = True
        header = True
        while writing_rows:
            if compress:
                filename = self.file_prefix + "_{:0>3d}.csv.gz".format(csv_num)
                current_file = gzip.open(filename, "wb", 5)
            else:
                filename = self.file_prefix + "_{:0>3d}.csv".format(csv_num)
                current_file = open(filename, "wb")
            print('writing file {}'.format(filename))
            writer = csv.DictWriter(current_file,
                                    fieldnames=field_names,
                                    quoting=csv.QUOTE_NONNUMERIC)
            if header:
                writer.writeheader()
                header = False
            for row in df:
                writer.writerow(row)
                if current_file.tell() >= self.byte_chunk_size:
                    current_file.close()
                    csv_num += 1
                    break
            else:
                writing_rows = False
            current_file.close()
